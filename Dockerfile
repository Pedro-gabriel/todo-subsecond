FROM node:current-slim

WORKDIR /usr/src/app

COPY package.json .

RUN npm install

RUN export DATABASE_URL=sqlite:./todo-subsecond.sqlite

CMD ['npm', 'start']

COPY . .
